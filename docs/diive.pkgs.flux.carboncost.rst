diive.pkgs.flux.carboncost package
==================================

Submodules
----------

diive.pkgs.flux.carboncost.criticalheatdays module
--------------------------------------------------

.. automodule:: diive.pkgs.flux.carboncost.criticalheatdays
   :members:
   :undoc-members:
   :show-inheritance:

diive.pkgs.flux.carboncost.figures module
-----------------------------------------

.. automodule:: diive.pkgs.flux.carboncost.figures
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: diive.pkgs.flux.carboncost
   :members:
   :undoc-members:
   :show-inheritance:
