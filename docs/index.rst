.. diive documentation master file, created by
   sphinx-quickstart on Thu Dec 30 23:38:54 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to diive's documentation!
=================================

.. note::

   This project is under active development.

.. toctree::
   :maxdepth: 5
   :caption: Contents:

   common

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
